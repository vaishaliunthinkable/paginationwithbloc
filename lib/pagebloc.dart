//bloc class
import 'package:bloc/bloc.dart';

enum PageEvents { pageLoad,
  pageError,
  pageEmpty,
  firstEmpty,
  firstLoad,
  firstError,}
class PageState {
 String value1;
 PageState(String value1)
 {
   this.value1=value1;
 }

}
class PageBloc extends Bloc<PageEvents, PageState>{
  PageBloc(int initialState) : super(PageState("pageLoad"));

  @override
  // TODO: implement initialState
  int get initialState => 0;

  @override
  Stream<PageState> mapEventToState(PageEvents event) async*{
    // TODO: implement mapEventToState
    switch(event){
      case PageEvents.pageError:
        yield PageState("pageError") ;
        break;
      case PageEvents.firstError:
        yield PageState("firstError") ;
        break;
      case PageEvents.pageLoad:
        yield PageState("pageLoad") ;
        break;
      case PageEvents.pageEmpty:
        yield PageState("pageEmpty") ;
        break;
      case PageEvents.firstLoad:
        yield PageState("firstLoad") ;
        break;
      case PageEvents.firstEmpty:
        yield PageState("firstEmpty") ;
        break;
    }
  }

}