library pagination;

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pagination/pagebloc.dart';

typedef PaginationBuilder<T> = Future<List<T>> Function(int currentListSize);

class PaginationList<T> extends StatefulWidget {
  const PaginationList({
    Key key,
    @required this.itemBuilder,
    @required this.onError,
    @required this.onEmpty,
    @required this.pageFetch,
    this.scrollDirection = Axis.vertical,
    this.shrinkWrap = false,
    this.padding = const EdgeInsets.all(0),
    this.initialData = const [],
    this.physics,
    this.separatorWidget = const SizedBox(height: 0, width: 0),
    this.onPageLoading = const Center(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: SizedBox(
          height: 25,
          width: 25,
          child: const CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
            backgroundColor: Colors.amber,
          ),
        ),
      ),
    ),
    this.onLoading = const SizedBox(
      height: 25,
      width: 25,
      child: const CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
        backgroundColor: Colors.amber,
      ),
    ),
  }) : super(key: key);

  final Axis scrollDirection;
  final bool shrinkWrap;
  final EdgeInsets padding;
  final List<T> initialData;
  final PaginationBuilder<T> pageFetch;
  final ScrollPhysics physics;
  final Widget Function(BuildContext, T) itemBuilder;
  final Widget onEmpty;
  final Widget Function(dynamic) onError;
  final Widget separatorWidget;
  final Widget onPageLoading;
  final Widget onLoading;

  @override
  _PaginationListState<T> createState() => _PaginationListState<T>();
}

class _PaginationListState<T> extends State<PaginationList<T>>
    with AutomaticKeepAliveClientMixin<PaginationList<T>> {
  final List<T> _itemList = <T>[];
  dynamic _error;
  final StreamController<PageState> _streamController =
      StreamController<PageState>();

  @override
  void initState() {
    _itemList.addAll(widget.initialData);
    if (widget.initialData.length > 0) _itemList.add(null);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _Bloc = BlocProvider.of<PageBloc>(context);
    super.build(context);
    return

     BlocBuilder<PageBloc, PageState>(
        builder: (BuildContext context, PageState state) {
          _itemList.length == 0? _Bloc.add(PageEvents.firstLoad) : _Bloc.add(PageEvents.pageLoad);
          if (_itemList==null) {
          return widget.onLoading;
          }
          if (state.value1=="firstLoad") {
            int index=0;
            widget.pageFetch(index - widget.initialData.length).then(
                  (List<T> list) {
                if (_itemList.contains(null)) {
                  _itemList.remove(null);
                }
                list = list ?? <T>[];
                if (list.isEmpty) {
                  if (index == 0) {
                    _Bloc.add(PageEvents.firstEmpty);
                  } else {
                    _Bloc.add(PageEvents.pageEmpty);
                  }
                  return;
                }

                _itemList.addAll(list);
                _itemList.add(null);
                _Bloc.add(PageEvents.pageLoad);
              },
              onError: (dynamic _error) {
                this._error = _error;
                if (index == 0) {
                  _Bloc.add(PageEvents.firstError);
                } else {
                  if (!_itemList.contains(null)) {
                    _itemList.add(null);
                  }
                  _Bloc.add(PageEvents.pageError);
                }
              },
            );
          return widget.onLoading;
          }
          if (state.value1=="firstEmpty") {
          return widget.onEmpty;
          }
          if (state.value1=="firstError") {
          return widget.onError(_error);
          }
          return Center(
            child:


               ListView.separated(
                  itemBuilder: (BuildContext context, int index) {
                    if (_itemList[index] == null &&
                        state.value1 == "pageLoad") {

                        widget.pageFetch(index - widget.initialData.length).then(
                              (List<T> list) {
                            if (_itemList.contains(null)) {
                              _itemList.remove(null);
                            }
                            list = list ?? <T>[];
                            if (list.isEmpty) {
                              if (index == 0) {
                                _Bloc.add(PageEvents.firstEmpty);
                              } else {
                                _Bloc.add(PageEvents.pageEmpty);
                              }
                              return;
                            }

                            _itemList.addAll(list);
                            _itemList.add(null);
                            _Bloc.add(PageEvents.pageLoad);
                          },
                          onError: (dynamic _error) {
                            this._error = _error;
                            if (index == 0) {
                              _Bloc.add(PageEvents.firstError);
                            } else {
                              if (!_itemList.contains(null)) {
                                _itemList.add(null);
                              }
                              _Bloc.add(PageEvents.pageError);
                            }
                          },
                        );

                      return widget.onPageLoading;
                    }
                    if (_itemList[index] == null &&
                        state.value1 == "pageError") {
                      return widget.onError(_error);
                    }
                    return widget.itemBuilder(
                      context,
                      _itemList[index],
                    );
                  },
                  shrinkWrap: widget.shrinkWrap,
                  scrollDirection: widget.scrollDirection,
                  physics: widget.physics,
                  padding: widget.padding,
                  itemCount: _itemList.length,
                  separatorBuilder: (BuildContext context, int index) =>
                  widget.separatorWidget,
                )

            );

            //Text('$state', style: Theme.of(context).textTheme.headline1),

        },
      );



  }



  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;
}


